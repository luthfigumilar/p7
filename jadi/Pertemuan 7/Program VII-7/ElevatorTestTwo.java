public class ElevatorTestTwo
{
	public static void main(String args[])
	{
		Elevator myElevator = new Elevator();
		myElevator.openDoor();
		myElevator.closeDoor();
		myElevator.goUp();
		myElevator.goUp();
		myElevator.goUp();
		myElevator.goDown();
		myElevator.closeDoor();
		myElevator.goDown();
		myElevator.openDoor();
		myElevator.closeDoor();
		myElevator.goDown();
		myElevator.setFloor(myElevator.TOP_FLOOR);
		myElevator.openDoor();
		int curFloor = myElevator.getFloor();
		System.out.println("Current Floor : " + curFloor);
		myElevator.openDoor();
	}
}