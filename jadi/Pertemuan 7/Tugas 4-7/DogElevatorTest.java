import java.util.Scanner;
public class DogElevatorTest
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		Elevator myElevator = new Elevator();
		Dog buddy = new Dog();

		System.out.print("Masukan Berat anjing : "); buddy.setWeight(scan.nextInt()); 
		myElevator.openDoor();
		myElevator.closeDoor();
		myElevator.goUp();
		myElevator.goUp();
		myElevator.goUp();
		myElevator.goDown();
		myElevator.closeDoor();
		myElevator.goDown();
		myElevator.openDoor();
		myElevator.closeDoor();
		myElevator.goDown();
		myElevator.setFloor(myElevator.TOP_FLOOR);
		myElevator.openDoor();
		int curFloor = myElevator.getFloor();
		myElevator.openDoor(); 

		System.out.println("\n\n\nBerat Anjing Adalah : "+ buddy.getWeight());
		System.out.println("Current Floor : " + curFloor);
	}
}